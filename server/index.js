const Hapi = require('hapi');
const Path = require('path');

class EchoServerHapi{
    constructor(args){
        this.server = Hapi.server(args);
    }
    async init(){
        try {
          this.server.register(require('inert'));
          await this.routing();
          await this.server.start();
          console.log(`Here we have the echo Server Hapi running:: ${this.server.info.uri}`);
        } catch (error) {
          console.log('Error while trying to run echo Server Hapi:: ' + error.message);
        }
    }
    async routing(){
        this.server.route({
            method: 'GET',
            path: '/msg',
            handler: (request, h) => {
                return JSON.stringify('Hello, Javascript Developer');
            }
        });        
        this.server.route({
            method: 'GET',
            path: '/msg/{name}',
            handler: (request, h) => {
                return JSON.stringify('Hello, ' + encodeURIComponent(request.params.name) );
            }
        });
        this.server.route({
            method: 'POST',
            path: '/msg',
            handler: (request, h) => {
                return JSON.stringify(request.payload);
            }
        });
        this.server.route({
            method: 'PUT',
            path: '/msg',
            handler: (request, h) => {
                return JSON.stringify(request.payload);
            }
        });
        this.server.route({
            method: 'DELETE',
            path: '/msg/{name}',
            handler: (request, h) => {
                return JSON.stringify( 'Bye, ' + encodeURIComponent(request.params.name) );
            }
        });
        this.server.route({
            method: 'GET',
            path: '/{param*}',
            handler: {
                directory: {
                    path: '.',
                    redirectToSlash: true,
                    index: true,
                }
            }
        });        
    }
}
const firstEchoServer = new EchoServerHapi({
    port: process.env.API_PORT,
    host: '0.0.0.0',
    app: {},
    routes: {
        files: {
            relativeTo: Path.join(__dirname, 'public')
        }
    }
});
firstEchoServer.init();
